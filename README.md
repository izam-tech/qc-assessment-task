

## Challenge Idea
The main idea of the task to evaluate how writing test cases and how you will write the clean code script in automation

We have to sections in our task

- `Stories` Read the five stories that already attached with our task here in pdf file stories.pdf.
- `Automation part` check below our subdomain credentials to complete your automation script.


## Automation task details

- You need to automate the following on ​"https://automationtesting.daftara.com/v2/owner/departments" , using selenuim webdriver (Java):   
	1.Create new department.
	
	2.open list of departments and search for cretaed department by name and status.
	
	3.Open selected department and edit on any field then save 

- Access credentials
	* siteurl  :  https://automationtesting.daftra.com
	* username : automationtesting@test.com
	* password : 123456
	
	
## Acceptance Criteria

- Test cases formatting
- Test cases coverage
- Tests script should be independent from testing data
- Develop test script with Testng framework 

## Expectations

Task will be evaluated based on

1. Code quality

2. The automation code should be delivered via any git server

3. Report of test cases status to be pushed in same branch


## How to push your code

1. Create Bitbucket or Github account
2. Create folder with your name for example mohamed-saied
3. Push your code by the following command
	- git add .
	- git commit -m "Your messages"
	- git push
4. Send the link repo by email 

Good Luck !


	
